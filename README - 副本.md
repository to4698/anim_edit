# 3dsMax 动画重定向 v0.42 | 2021.03.06

[B站视频-3dsMax 动画重定向 v0.42 更新演示](https://www.bilibili.com/video/bv14V411Y7J6)

[B站视频-3dsMax重定向_FBX动画复制_FBXtoBiped-这个账号弃用](https://www.bilibili.com/video/BV1r541157hs?p=1)

![](https://gitee.com/to4698/anim_edit/raw/master/000456.png)

---



## 使用


1.   拾取蒙皮骨骼或者绑定控制器 到自定义面板 中
2.   拾取另外一角色的 蒙皮骨骼 或者 绑定控制器 到控制面板 中
3.   保存匹配Pose
4.   保存自定义模板(可选步骤)
5.   保存控制模板(可选步骤)
6.   设置复制动画类型(默认除质心外全为复制旋转)
7.   保存角色

完成以上 7 步，即可在自定义角色和控制角色之间互相 复制动画

----
## 详细文档

0.42版本新加了 FBXSDK ,安装相对多了一个步骤,请仔细看安装说明。

*   [安装](https://gitee.com/to4698/anim_edit/wikis/%E4%B8%8B%E8%BD%BD%E5%AE%89%E8%A3%85?sort_id=3631157)

*   [更新历史](https://gitee.com/to4698/anim_edit/wikis/%E6%9B%B4%E6%96%B0%E5%8E%86%E5%8F%B2?sort_id=3631161)

*   [保存匹配Pose](https://gitee.com/to4698/anim_edit/wikis/%E4%BF%9D%E5%AD%98%E5%8C%B9%E9%85%8DPose?sort_id=3631158)

*   [设置和模板](https://gitee.com/to4698/anim_edit/wikis/%E8%AE%BE%E7%BD%AE%E5%92%8C%E6%A8%A1%E6%9D%BF?sort_id=3631162)

*   [bip动画导入到自定义绑定](https://gitee.com/to4698/anim_edit/wikis/bip%E5%8A%A8%E7%94%BB%E5%AF%BC%E5%85%A5%E5%88%B0%E8%87%AA%E5%AE%9A%E4%B9%89%E7%BB%91%E5%AE%9A?sort_id=3631156)

*	[FBX动画批量重定向](https://gitee.com/to4698/anim_edit/wikis/%E6%89%B9%E9%87%8F%E9%87%8D%E5%AE%9A%E5%90%91FBX%E5%8A%A8%E7%94%BB?sort_id=3663589)

*   [想将FBX动画转成.bip](https://gitee.com/to4698/anim_edit/wikis/%E6%83%B3%E5%B0%86FBX%E5%8A%A8%E7%94%BB%E8%BD%AC%E6%88%90.bip?sort_id=3631159)

*   [故障排除-错误代码](https://gitee.com/to4698/anim_edit/wikis/%E6%95%85%E9%9A%9C%E6%8E%92%E9%99%A4?sort_id=3631160)
---

## 百度云下载


*   3dsMax 2015 -2016
*   3dsMax 2017
*   3dsMax 2018-2019
*   3dsMax 2020

因为3dsMAX-PySide 奇葩的BUG,042版本不支持2020,043版将会分出 2020版本

链接：https://pan.baidu.com/s/1oakAx_enc_C957trwY-BOQ
提取码：0b0w

自行选择对应版本下载,(创建Biped功能只能在英文版本MAX中使用)

-----

群号：109185941

点击链接加入群聊【天晴工具组】：https://jq.qq.com/?_wv=1027&k=YJSgv3fe

E-Mail: 738746223@qq.com

99U : 199505